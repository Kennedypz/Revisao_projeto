from django.forms import ModelForm
from .models import contact

class ContactForm(ModelForm):
    class Meta:
        model = contact
        fields = ['name', 'telephone', 'email', 'birthdt', 'cpf', 'photo']
