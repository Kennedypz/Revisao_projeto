from django.shortcuts import render, redirect, get_object_or_404
from .forms import *
from .models import Contact

#CRUD

#Create

def contact_new(request, template_name = 'contact_form.html'):
    form = ContactForm(request.POST or None, request.file or None)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('contact.list')
    return render(request, template_name, dados)

#Read

def contact_list(request, template_name = 'contact.html'):
    contacts = Contact.objects.all()

    dados = {'contacts': contacts}

    return render(request, template_name, dados)

#Update

def contact_update(request, id, template_name='contact_form.html'):
    contact = get_object_or_404(Contact, pk=id)
    form = ContactForm(request.POST or None, request.file or None, instance=contact)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('contact_list')
    return render(request, template_name, dados)

#Delete
def contact_delete(request, id, template_name='contact_delete.html'):
    contact = get_object_or_404(Contact, pk=id)

    if request.method == 'POST':
        contact.delete()
        return redirect('contacts_list')